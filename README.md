This directory contains the gccxml-plugin.  
The gccxml-plugin is a drop-in replacement for deprecated Kitware's gccxml. The plugin code is based on the xml.c file from the original gccxml, adapted to work inside the gcc as a  plugin.  
It may be useful in case you just want to parse/analyse/wrap a few headers and do not plan to use clang/llvm stuff for anything else.

Status:
- All the language dialects, features and extensions, implemented in the current gcc version, are supported and could be dumped to xml, as we are reusing existing gcc codebase.
- Early alpha, may contain bugs :)!

TODO:
- c++11/14 features support in xml dump. Could be added in castxml-compatible way or in user-defined way. Feel free to add a feature request.
- Code clean up and refactoring.
- Plain c, fortran dumps.

Not supported:
- Other compiler emulation.
- Windows version. Patches are welcome.
- Attributes. Not hard to add, just not a priority. Patches are welcome here also.

Requrements:
- cmake >=2.8.12
- gcc >=4.8
- binutils-devel, gcc-plugin-devel for package based distributions like Fedora

Installation:
- cmake -DCMAKE_INSTALL_PREFIX="some/install/path" -DCMAKE_BUILD_TYPE=Release
- make install

Usage:  
gccxml -fxml=<dump_file.xml> [-fxml-start=...] [other gcc options].  
-fxml option is mandatory.