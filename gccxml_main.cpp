/* 
Simple wrapper, compatible with original gccxml

This file is part of gccxml-plugin.

Copyright (C) 2015 Nick Krylov

This file is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; see the file COPYING3.  If not see
<http://www.gnu.org/licenses/>.
 */
#include <unistd.h>

#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include <iterator>

using namespace std;
static string gcc_ver
#ifdef GCC_VER
        = "-" GCC_VER
#endif
;
#ifndef PLUGIN_NAME
#define PLUGIN_NAME "gccxml_plugin"
#endif

#ifndef SO_EXT
#define SO_EXT ".so"
#endif

//FULL path with trailing slash
static string plugin_path
#ifdef PLUGIN_PATH
        = PLUGIN_PATH
#endif
;

static bool extract_opt(char** argv, int& i, int argc, const string& opt_name, string&opt_val) {
    string arg = argv[i];
    string::size_type pos = arg.find(opt_name);
    if (pos != string::npos) {
        //check inline or sep opt
        string::size_type fnpos = pos + opt_name.length();
        if (arg.length() > fnpos) {
            //inline
            opt_val = arg.substr(fnpos + 1);
        } else {
            //next to
            if (i < argc) {
                opt_val = argv[i + 1];
                i++;
            } else {
                cerr << opt_name << " option requires filename argument!" << endl;
                exit(1);
            }
        }
        return true;
    }
    return false;
}

int main(int argc, char** argv) {
    //Should run gcc plugin compiled for

    typedef vector<char const *> VectorCharP;
    VectorCharP args;
    string fxml_name, fxml_start;
    string gpp_name = "g++" + gcc_ver;
    args.push_back(gpp_name.c_str());
    args.push_back("-x");
    args.push_back("c++");
    bool show_cmdl = false;
    char*argvi =NULL;
    for (int i = 1; i < argc; i++) {
        argvi = argv[i];

        if(string(argvi).find("-info")!=string::npos){
            show_cmdl = true;
            continue;
        }
        if (extract_opt(argv, i, argc, "fxml-start", fxml_start)) {
            continue;
        }
        if (extract_opt(argv, i, argc, "fxml", fxml_name)) {
            continue;
        }

        args.push_back(argvi);

    }
    if (fxml_name.empty()) {
        cerr << "-fxml option must be set!" << endl;
        return EXIT_FAILURE;
    }

    string plugin_file = string("-fplugin=")+plugin_path + PLUGIN_NAME + SO_EXT; //TODO: C/C++ Switch

    args.push_back(plugin_file.c_str());
    string fxml_arg=string("-fplugin-arg-"PLUGIN_NAME"-fxml=")+fxml_name;
    args.push_back(fxml_arg.c_str());
    string fxml_start_arg;
    if (!fxml_start.empty()) {
        fxml_start_arg=string("-fplugin-arg-"PLUGIN_NAME"-fxml-start=")+fxml_start;
        args.push_back(fxml_start_arg.c_str());
    }

    args.push_back("-pipe");
    args.push_back("-S");
    args.push_back(NULL); //MUST be the last one

    if(show_cmdl){
        cout<<"gcc_ver "<<gcc_ver<<endl;
        cout<<"PLUGIN_NAME "<<PLUGIN_NAME<<endl;
        cout<<"SO_EXT "<<SO_EXT<<endl;
        cout<<"plugin_path "<<plugin_path<<endl;
        cout<<"plugin_file "<<plugin_file<<endl;
        copy(args.begin(), args.end() - 1, ostream_iterator<char const *>(cerr, " "));
        cout<<endl;
        return EXIT_SUCCESS;
    }
    return execvp(gpp_name.c_str(), (char**) &args[0]);
}
