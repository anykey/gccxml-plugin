/* 
Glue code connecting xml dumping part with running gcc instance

This file is part of gccxml-plugin.

Copyright (C) 2015 Nick Krylov

This file is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; see the file COPYING3.  If not see
<http://www.gnu.org/licenses/>.
 */
#include <iostream>

#include "gcc-plugin.h"
#include "plugin-version.h"
#include "diagnostic.h"
#include "options.h"

using namespace std;

static bool be_verbose = verbose_flag;

namespace {
    int pargc;
    plugin_argument *pargv;
}
//Fwd decl
void
do_xml_output(const char* filename, const char* flag_xml_start);

extern "C" {
    int plugin_is_GPL_compatible;

    void
    gate_callback(void*, void*) {
        // If there were errors during compilation,
        // let GCC handle the exit.
        //
        if (errorcount || sorrycount)
            return;

        if(be_verbose) cout << "processing: '" << main_input_filename<<"'" << endl;

        // Parse options.

        string flag_xml_start, fxml;
        for (int i = 0; i < pargc; i++) {
            char*k = pargv[i].key;
            char*v = pargv[i].value;
            if (string("fxml") == k) {
                fxml = v;
            }
            if (string("fxml-start") == k) {
                flag_xml_start = v;
            }
        }

        // Dump AST. Signal error if any

        if (!fxml.empty()) {
            do_xml_output(fxml.c_str(), flag_xml_start.empty() ? NULL : flag_xml_start.c_str());
            return exit(0);
        }
        return exit(FATAL_EXIT_CODE);
    }

    extern "C" int
    plugin_init(plugin_name_args* info,
            plugin_gcc_version* version) {
        int r(0);

        if (!plugin_default_version_check(version, &gcc_version)) {
            cerr << "Version mismatch!" << endl;
            return 1;
        }
        if(be_verbose) cout << "starting " << info->base_name << endl;
        
        pargc = info->argc;
        pargv = info->argv;
        asm_file_name = HOST_BIT_BUCKET;

        // Register callbacks.
        register_callback(info->base_name,
                PLUGIN_OVERRIDE_GATE,
                &gate_callback,
                0);
        return r;
    }
};
