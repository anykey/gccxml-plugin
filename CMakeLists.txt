cmake_minimum_required( VERSION 2.8.12 )

set(PROJECT_NAME gccxml_plugin)
project(${PROJECT_NAME})

if(NOT CMAKE_COMPILER_IS_GNUCC)
    message(FATAL_ERROR "GCC compiler NOT found!")
endif()

if (CMAKE_CXX_COMPILER_VERSION VERSION_LESS 4.8)
    message(FATAL_ERROR "GCC version must be at least 4.8!")
endif()

#begin snippet based on code from #from https://blogs.kde.org/2009/05/09/progress-gcc-plugins
execute_process(COMMAND ${CMAKE_C_COMPILER} "-print-file-name=plugin/include/gcc-plugin.h"
    OUTPUT_VARIABLE gccpluginh_path
    OUTPUT_STRIP_TRAILING_WHITESPACE
    ERROR_VARIABLE gccpluginh_error 
)

if(gccpluginh_error OR (NOT EXISTS "${gccpluginh_path}"))
    message(FATAL_ERROR "Cant find gcc-plugin.h. Check your GCC installation (i.e. -devel packages installed)!")
endif()

get_filename_component(GCCPLUGINS_INCLUDE_DIR "${gccpluginh_path}" DIRECTORY)
#end snippet 

include_directories( ${GCCPLUGINS_INCLUDE_DIR} )

unset(GCC_VEXEC CACHE)
#Try to find versioned compiler executable .
find_file(GCC_VEXEC NAMES "gcc-${CMAKE_CXX_COMPILER_VERSION}")

#PLUGIN
set( plugin_SRCS gccxml_dump.cpp  gccxml_plugin.cpp )
add_library( ${PROJECT_NAME} MODULE ${plugin_SRCS} )

set_target_properties( ${PROJECT_NAME} PROPERTIES PREFIX "" )
install(TARGETS ${PROJECT_NAME} LIBRARY DESTINATION lib )

#WRAPPER
set(WRAPPER gccxml)
add_executable(${WRAPPER} gccxml_main.cpp )

file(TO_NATIVE_PATH "${CMAKE_INSTALL_PREFIX}/lib/" PLUGIN_DIR)

target_compile_definitions(${WRAPPER} PUBLIC SO_EXT="${CMAKE_SHARED_LIBRARY_SUFFIX}" PLUGIN_NAME="${PROJECT_NAME}" PLUGIN_PATH="${PLUGIN_DIR}")

if (GCC_VEXEC)
    target_compile_definitions(${WRAPPER} PUBLIC GCC_VER="${CMAKE_CXX_COMPILER_VERSION}")
endif()

install(TARGETS ${WRAPPER} RUNTIME DESTINATION bin )

set(CPACK_GENERATOR TGZ)
set(CPACK_PACKAGE_NAME ${PROJECT_NAME})
set(CPACK_SOURCE_GENERATOR "TBZ2")

INCLUDE(CPack)
add_custom_target(dist COMMAND ${CMAKE_MAKE_PROGRAM} package_source)
